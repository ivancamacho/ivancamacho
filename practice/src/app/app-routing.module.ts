import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
{ path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule' },
{ path: 'compartido', loadChildren: './modules/compartido/compartido.module#CompartidoModule' },
{ path: 'principal', loadChildren: './modules/principal/principal.module#PrincipalModule' } ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
