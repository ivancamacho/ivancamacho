import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  listHobbies = ['Watch TV', 'Listen Music'];

  // Form
  practiceForm: FormGroup;
  show = false;
  message = '';

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.practiceForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
  }

  send() {
    this.show = true;
    if (this.practiceForm.valid) {
      this.message = 'Information sent';
    } else {
      this.message = 'Please enter your name';
    }
  }
}
