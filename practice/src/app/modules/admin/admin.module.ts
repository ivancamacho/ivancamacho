import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponentComponent } from './admin-component/admin-component.component';
import { RouterModule } from '@angular/router';

const ROUTE = [
  { path: '', component: AdminComponentComponent },
];

@NgModule({
  declarations: [AdminComponentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTE)
  ]
})
export class AdminModule { }
