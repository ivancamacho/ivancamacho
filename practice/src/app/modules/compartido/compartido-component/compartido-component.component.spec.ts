import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompartidoComponentComponent } from './compartido-component.component';

describe('CompartidoComponentComponent', () => {
  let component: CompartidoComponentComponent;
  let fixture: ComponentFixture<CompartidoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompartidoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompartidoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
