import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompartidoComponentComponent } from './compartido-component/compartido-component.component';
import { RouterModule } from '@angular/router';

const ROUTE = [
  { path: '', component: CompartidoComponentComponent },
];

@NgModule({
  declarations: [CompartidoComponentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTE)
  ]
})
export class CompartidoModule { }
