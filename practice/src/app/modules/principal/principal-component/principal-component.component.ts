import { AfterViewInit, Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/shared/global/global-service';

@Component({
  selector: 'app-principal-component',
  templateUrl: './principal-component.component.html',
  styleUrls: ['./principal-component.component.scss']
})
export class PrincipalComponentComponent implements OnInit, AfterViewInit {

  logged = false;

  constructor(private global: GlobalService ) { }

  ngOnInit() {
    this.global.setLogin(true);
  }

  ngAfterViewInit(): void {
      this.global.getLogin().subscribe(result => {
        if (result) {
          this.logged = result;
        }
      });
  }

}
