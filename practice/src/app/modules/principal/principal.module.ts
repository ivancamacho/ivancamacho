import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalComponentComponent } from './principal-component/principal-component.component';
import { RouterModule } from '@angular/router';

const ROUTE = [
  { path: '', component: PrincipalComponentComponent },
];

@NgModule({
  declarations: [PrincipalComponentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTE)
  ]
})
export class PrincipalModule { }
