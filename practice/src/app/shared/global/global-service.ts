import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    private isLogged = new Subject<boolean>();

    getLogin(): Observable<boolean> {
        return this.isLogged.asObservable();
    }

    setLogin(valor: boolean) {
        this.isLogged.next(valor);
    }

}
