import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  async connectToApi(url: string, type: peticionType, params ?: any): Promise<any> {
    let headers: any;
    return await new Promise(async (resolve, reject) => {
      switch (type) {
        case peticionType.POST:
          headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
          this.http.post(url, params, {headers: headers}).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
          break;
          case peticionType.POST_FORM:
            headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded ; charset = UTF-8'});
            this.http.post(url, params, {headers: headers}).pipe(map(res => res))
              .subscribe((data) => {
                resolve(data);
              }, (err) => {
                reject(err);
              });
            break;
        case peticionType.POST_MULTIPART:
          headers = new HttpHeaders({'Content-Type': 'multipart/form-data'});
          this.http.post<any>(url, params, {headers: headers}).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
          break;
        case peticionType.POST_JSON:
          headers = new HttpHeaders({'Content-Type': 'application/json'});
          await this.http.post(url, JSON.stringify(params), {headers: headers}).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
          break;
          case peticionType.POST_JSON_FILE:
            headers = new HttpHeaders({'Content-Type': 'application/json'});
            await this.http.post(url, JSON.stringify(params), {headers: headers, responseType : 'blob'}).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
            break;
        case peticionType.GET_JSON:
          headers = new HttpHeaders({'Content-Type': 'application/json'});
          await this.http.get(url, {headers: headers}).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
          break;
        case peticionType.GET_SINHEADERS:
          await this.http.get(url).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
          break;
        case peticionType.GET_TEXT:
          headers = new HttpHeaders({'Content-Type':'text/plain'});
          this.http.get(url, {headers: headers}).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
          break;
        case peticionType.POST_TEXT:
          headers = new HttpHeaders({'Content-Type':'text/plain', responseType: 'text'});
          this.http.post(url, params, {responseType: 'text'})
            .subscribe((data) => {
              resolve(data);
            },(err) => {
              reject(err);
            });
          break;
        case peticionType.POST_SINHEADERS:
          this.http.post(url, params).pipe(map(res => res))
            .subscribe((data) => {
              resolve(data);
            }, (err) => {
              reject(err);
            });
          break;
      }
    });
  }

}

export enum peticionType {
  POST, POST_FORM, POST_MULTIPART, POST_JSON, POST_JSON_FILE,
  GET_JSON, GET_TEXT, POST_TEXT, POST_SINHEADERS, GET_SINHEADERS
}
